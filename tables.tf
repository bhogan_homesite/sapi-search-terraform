provider "aws" {
  version = "~> 2.0"
  region  = var.provider_region
}

module "offerset_table" {
  source = "./dynamodb"
  dynamodb_table_name               = "${var.dynamodb_table_name_prefix}Offerset"                
  dynamodb_table_read_capacity      = 1
  dynamodb_table_write_capacity     = 1
  hash_key       = "OfferSetId"
  #range_key      = "PartnerKey"

  attributes = {
    "OfferSetId" = "S"
    #,"PartnerKey" = "S"
  }
}

module "questions_table" {
  source = "./dynamodb"
  dynamodb_table_name               = "${var.dynamodb_table_name_prefix}Questions"                
  dynamodb_table_read_capacity      = 1
  dynamodb_table_write_capacity     = 1
  hash_key       = "InquiryId"
  #range_key      = "Product"

  attributes = {
    "InquiryId" = "S"
    #,"Product" = "S"
  }
}

module "applicationquestions_table" {
  #source = "git@bitbucket.org:p20/dynamodb.git?ref=CD-2553"
  source = "./dynamodb"
  dynamodb_table_name               = "${var.dynamodb_table_name_prefix}ApplicationQuestions"                
  dynamodb_table_read_capacity      = 1
  dynamodb_table_write_capacity     = 1
  hash_key       = "ApplicationQuestionId"

  attributes = {
    "ApplicationQuestionId" = "S",
  }
}

module "choiceid_table" {
  source = "./dynamodb"
  dynamodb_table_name               = "${var.dynamodb_table_name_prefix}ChoiceID"                
  dynamodb_table_read_capacity      = 1
  dynamodb_table_write_capacity     = 1
  hash_key       = "ChoiceId"

  attributes = {
    "ChoiceId" = "S",
  }
}

module "taskstatus_table" {
  source = "./dynamodb"
  dynamodb_table_name               = "${var.dynamodb_table_name_prefix}TaskStatus"                
  dynamodb_table_read_capacity      = 1
  dynamodb_table_write_capacity     = 1
  hash_key       = "InquiryId"
  #range_key      = "TaskName"

  attributes = {
    "InquiryId" = "S"
    #,"TaskName" = "S"
  }
}

module "questionstatus_table" {
  source = "./dynamodb"
  dynamodb_table_name               = "${var.dynamodb_table_name_prefix}QuestionStatus"                
  dynamodb_table_read_capacity      = 1
  dynamodb_table_write_capacity     = 1
  hash_key       = "InquiryId"

  attributes = {
    "InquiryId" = "S",
  }
}

output "offerset_table_arn" {
  value = module.offerset_table.dynamodb_arn
}

output "questions_table_arn" {
  value = module.questions_table.dynamodb_arn
}

output "applicationquestions_table_arn" {
  value = module.applicationquestions_table.dynamodb_arn
}

output "choiceid_table_arn" {
  value = module.choiceid_table.dynamodb_arn
}

output "taskstatus_table_arn" {
  value = module.taskstatus_table.dynamodb_arn
}

output "questionstatus_table_arn" {
  value = module.questionstatus_table.dynamodb_arn
}

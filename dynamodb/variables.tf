variable "dynamodb_table_name" {
  type    = string
}

variable "dynamodb_table_read_capacity" {
  type    = number
}

variable "dynamodb_table_write_capacity" {
  type    = number
}

variable "hash_key" {
  type    = string
}

variable "range_key" {
  type    = string
  default = null
}

variable "attributes" {
    type  = map
}
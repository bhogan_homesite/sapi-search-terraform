variable "dynamodb_table_name_prefix" {
  type    = string
  default = "bhogan_"
}

variable "provider_version"{
    type = string
    default = "~> 2.0"
}

variable "provider_region"{
    type = string
    default = "us-east-1"
}

variable "aws_region" {
  description = "The AWS region to create things in."
  default     = "us-east-1"
}

